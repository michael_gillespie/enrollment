what does it do?
enrollment.gsheet is used to send alerts when a new student arrives at the school and needs a computer.
After a new student meets the requirements (payment and has signed forms) an email is genereated to sent 
to me so that I know to setup a new computer.

skills used:
> gsheets
> JS

requirements:
> web browser (tested on chrome)

how to run:
1. registrar adds a new student
2. requirements are met
3. email is sent and a new computer is setup
