function onEdit() {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getActiveSheet()

  // get last modified row
  var row = sheet.getLastRow();
  
  // get last modified values
  var code = sheet.getRange(row, 10).getValue();
  var form = sheet.getRange(row, 11).getValue();
  var paid = sheet.getRange(row, 9).getValue();

  // create email message
  var date_of_enrollment = sheet.getRange(row, 2).getValue();
  var er_code = sheet.getRange(row, 3).getValue();
  var student_name = sheet.getRange(row, 4).getValue();
  var student_number = sheet.getRange(row, 5).getValue();
  var last_school = sheet.getRange(row, 6).getValue();
  var grade = sheet.getRange(row, 7).getValue();
  var notes = sheet.getRange(row, 8).getValue();
  
  var message = student_name.concat("\nDate of Enrollment = ", date_of_enrollment,
                                    "\nE/R Code = ", er_code,
                                    "\nStudent Number = ", student_number,
                                    "\nLast School = ", last_school,
                                    "\nGrade = ", grade,
                                    "\nNotes = ", notes);
  
  if ((code == "X" || code == "x") && (form == "X" || form == 'x') && (paid == "X" || paid == 'x')) {
    // stop duplicate emails from being sent
    var email_already_sent = sheet.getRange(row, 12).getValue();
    if (email_already_sent != "email sent") {
      // send email via trigger: Resources > Current projects's triggers > OnEdit
      sendMail(message)
    }
    
    // set emailed flag
    sheet.getRange(row, 12).setValue("email sent");
    
    // set comments
    comment()
  }
  
  function sendMail(message) {
    MailApp.sendEmail("mgillespie@henrico.k12.va.us", "new student", message);
  }
  
  function comment() {
    var comment_range = sheet.getRange(1, 8, 20)
    comment_range.setComment("Leave this blank - this section is more of a status placeholder so that you know when I have requested a computer.");
  }
}
